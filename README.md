# Database 120

To submit your solution:
* Create a GitLab account
* Fork this repository
* Push your submission to your repository
* When you are done, open a merge request to this repository

Please add instructions how to use the scripts into this README file.
